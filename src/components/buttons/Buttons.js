import React, {useState}from 'react'

const Buttons = ({ getQuery, getData }) => {
    const [digit, setDigit] = useState('')

    const onClick = (q) => {
        setDigit(q)
        getQuery(q) 
    }

const calculation = (digit)=> {

    try {
        getData(String(eval(digit)))
    } catch (error) {
        setDigit('error')
        getQuery('I can not do that 4 U my Friend :)')
    
    }
    // var calcFunction;

    // (digit === 0 ) 
    //     ? 
    //         calcFunction = (digit) => { throw new Error('Invalid dividend ' + digit); }
    //     :
    //         calcFunction = getData(String(eval(digit)))

    // console.log(digit);
    // return calcFunction;
  }
  
    return (
            <div className="calc">

                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="7">7</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="8">8</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="9">9</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn operator' value="/">&divide;</button>

                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="4">4</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="5">5</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="6">6</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn operator' value="*">&times;</button>

                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="1">1</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="2">2</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="3">3</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn operator' value="-">-</button>

                <button onClick={() => onClick('')} type="button" className='calc-btn clear' value="0">C</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn' value="0">0</button>
                <button onClick={() => calculation(digit)} type="button" className='calc-btn equal-sign' value="=">=</button>
                <button onClick={(e) => onClick(digit + e.target.value)} type="button" className='calc-btn operator' value="+">+</button>

            </div>
    )
}

export default Buttons