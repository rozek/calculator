import React from 'react'

const Screen = ({ data }) => {

    return (
        <section className='calculator-screen'>
            <input 
            type="text" 
            value={data}
            disabled
            />
        </section>
    )
}

export default Screen
