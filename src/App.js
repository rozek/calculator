import React, {useState, useEffect} from 'react';
import './App.css';
import Screen from './components/screen/Screen'
import Buttons from './components/buttons/Buttons'


const App =()=> {
  const [data, setData] = useState('')
  const [query, setQuery] = useState('0')

  useEffect(()=> {

    const result =()=> {
      setData(query)
    }
    result()
    console.log('state data:' + data);
    console.log('state query:' + query);
    
  },[query])

  return (
    <div className='calc-module'>
    <div className="container">
      <Screen data={data}/> 
      <Buttons getQuery={(q) => setQuery(q)} getData= {(q) => setData(q)}/>
    </div>
    </div>
  );
}

export default App;
